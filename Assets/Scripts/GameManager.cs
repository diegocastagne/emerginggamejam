﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class GameManager : MonoBehaviour
{
    public int score = 0;
    public int MaxScore = 0;
    [SerializeField] private GameObject _WinScreen;
    [SerializeField] private TextMeshProUGUI _Button;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        if(score >= MaxScore)
        {
            Time.timeScale = 0;
            _WinScreen.SetActive(true);
        }
    }
    public void addScore()
    {
        score++;
         _Button.SetText(score.ToString());
    }
}
