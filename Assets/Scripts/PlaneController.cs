﻿
/*
    Copyright (C) Sabastian Peters 2020
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneController : MonoBehaviour
{

    // ## UNITY EDITOR ##
    
    [Tooltip("Speed of the plane in m/s")]
    [SerializeField] private float _speed = 10f;
    [Tooltip("How much faster do we move on rotate up / down")]
    [SerializeField] private float _pitchMultiplier = 1f;
    [Tooltip("How much faster do we move when barrel rolling")]
    [SerializeField] private float _rollMultiplier = 1f;
    [Tooltip("How much slower do we fall due to gravity")]
    [Range(0f, 1f)][SerializeField] private float _gravityMultiplier = 1f;
    
    // ## PRIVATE UTIL VARS ##
    
    Vector3 _oldTorque;
    Vector3 _oldVelocity;
    Rigidbody _rigidbody;
    Vector2 _moveInput;

    
    
    
    // ## UNITY EVENTS ##
    
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }


    private void Update()
    {
        _moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    private void FixedUpdate()
    {
        // changed torque
        // _rigidbody.AddRelativeTorque(-_oldTorque, ForceMode.Impulse); /// remove old torque
        _rigidbody.AddRelativeTorque(_oldTorque = new Vector3(_moveInput.y * _pitchMultiplier, 0, -_moveInput.x * _rollMultiplier), ForceMode.Impulse); /// add new torque

        // changes velocity
        _rigidbody.AddForce(_oldVelocity = (transform.forward * _speed), ForceMode.Impulse); /// add new velocity

        // combat gravity
        _rigidbody.AddForce(Physics.gravity * -_gravityMultiplier, ForceMode.Acceleration);
    }
    
    
    // ## PUBLIC METHODS ##
    
    
    // ## PRIVATE UITL METHODS ##
    
    
}
