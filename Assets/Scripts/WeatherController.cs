﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class WeatherController : MonoBehaviour
{
    private const string API_KEY = "bdd15ecb43df9ac98043c58853d756a6";

    [Header("City Info")]
    [SerializeField] private TMP_InputField _textInput = null;
    private string _cityName = "";

    [SerializeField] private ParticleSystem _snowSystem = null;
    [SerializeField] private ParticleSystem _rainSystem = null;
    [SerializeField] private ParticleSystem _cloudsSystem = null;

    [SerializeField] private float _checkWeatherRate = 10f;
    [SerializeField] private MapGenerator _mapGenerator = null;
    

    private float _countDown;
    private WeatherEnum _currentWeather;

    private void Awake()
    {
        _cityName = _textInput.text;
    }

    private void Start()
    {
        _countDown = _checkWeatherRate;
        StartCoroutine(GetWeather(CheckWeatherStatus));
    }

    private void Update()
    {
        _cityName = _textInput.text;
        
        _countDown -= Time.deltaTime;

        if(_countDown <= 0)
        {
            StartCoroutine(GetWeather(CheckWeatherStatus));
            _countDown = _checkWeatherRate;
        }
    }

    IEnumerator GetWeather(Action<WeatherInfo> onSuccess)
    {
        using (UnityWebRequest req = UnityWebRequest.Get(String.Format("http://api.openweathermap.org/data/2.5/weather?q={0}&APPID={1}", _cityName, API_KEY)))
        {
            yield return req.SendWebRequest();

            while (!req.isDone)
                yield return null;

            byte[] result = req.downloadHandler.data;
            string weatherJSON = System.Text.Encoding.Default.GetString(result);
            WeatherInfo info = JsonUtility.FromJson<WeatherInfo>(weatherJSON);

            onSuccess(info);
        }
    }

    [ContextMenu("Check For Snow")]
    public void CheckWeatherStatus(WeatherInfo info)
    {
        //bool snowing = info.weather[0].main.Equals("Snow");
        if(info.weather.Count > 0)
        {
            string weather = info.weather[0].main;
            WeatherEnum tempWeather = _currentWeather;

            switch (weather)
            {
                case "Rain":
                    _currentWeather = WeatherEnum.RAIN;
                    break;
                case "Snow":
                    _currentWeather = WeatherEnum.SNOW;
                    break;
                case "Clouds":
                    _currentWeather = WeatherEnum.CLOUDS;
                    break;
                default:
                    _currentWeather = WeatherEnum.CLEAR;
                    break;
            }

            if(tempWeather != _currentWeather)
                UpdateWeather();
        }
    }

    public void UpdateWeather()
    {
        var snowEmission = _snowSystem.emission;
        var rainEmission = _rainSystem.emission;
        var cloudsEmission = _cloudsSystem.emission;

        switch(_currentWeather)
        {
            case WeatherEnum.RAIN:
                rainEmission.rateOverTime = 7000f;
                cloudsEmission.rateOverTime = 200f;
                snowEmission.rateOverTime = 0f;

                _mapGenerator.regions[0].height = 0.4f;
                _mapGenerator.regions[1].height = 0.45f;
                _mapGenerator.regions[2].height = 0.6f;
                _mapGenerator.regions[3].height = 0.7f;
                break;
            case WeatherEnum.SNOW:
                rainEmission.rateOverTime = 0f;
                cloudsEmission.rateOverTime = 200f;
                snowEmission.rateOverTime = 5000f;

                _mapGenerator.regions[0].height = 0.2f;
                _mapGenerator.regions[1].height = 0.25f;
                _mapGenerator.regions[2].height = 0.45f;
                _mapGenerator.regions[3].height = 0f;
                break;
            case WeatherEnum.CLOUDS:
                rainEmission.rateOverTime = 0f;
                cloudsEmission.rateOverTime = 200f;
                snowEmission.rateOverTime = 0f;

                _mapGenerator.regions[0].height = 0.2f;
                _mapGenerator.regions[1].height = 0.3f;
                _mapGenerator.regions[2].height = 0.6f;
                _mapGenerator.regions[3].height = 0.7f;
                break;
            case WeatherEnum.CLEAR:
                rainEmission.rateOverTime = 0f;
                cloudsEmission.rateOverTime = 0f;
                snowEmission.rateOverTime = 0f;

                _mapGenerator.regions[0].height = 0.2f;
                _mapGenerator.regions[1].height = 0.3f;
                _mapGenerator.regions[2].height = 0.6f;
                _mapGenerator.regions[3].height = 0.75f;
                break;
        }

        _mapGenerator.GenerateMap();
    }
}
