﻿
/*
    Copyright (C) Sabastian Peters 2020
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowController : MonoBehaviour
{

    // ## UNITY EDITOR ##
    
    [SerializeField] private Transform _target;
    [Tooltip("Maximum move speed in meters/s")]
    [SerializeField] private float _moveSpeed = 10f;
    [Tooltip("Maximum rotation speed in degrees/s")]
    [SerializeField] private float _rotateSpeed = 15f;
    
    // ## PRIVATE UTIL VARS ##
    
    
    
    
    // ## UNITY EVENTS ##
    
    private void Update()
    {
        MoveTowardsTransform();
    }
    
    
    // ## PUBLIC METHODS ##
    
    
    // ## PRIVATE UITL METHODS ##
    

    private void MoveTowardsTransform ()
    {
        transform.position = Vector3.MoveTowards(transform.position, _target.position,_moveSpeed * Time.deltaTime);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, _target.rotation, _rotateSpeed * Time.deltaTime);
    }

    private void MatchTransform ()
    {
        transform.position = _target.position;
        transform.rotation = _target.rotation;
    }



    #if UNITY_EDITOR
    
    private void OnValidate()
    {
        if(_target == null) return;
        if(Application.isPlaying) return;
        MatchTransform();
    }

    #endif

}
