﻿using UnityEngine;
using System.Collections;

public class MapGenerator : MonoBehaviour
{

    public enum DrawMode { NoiseMap, ColourMap, Mesh };
    public DrawMode drawMode;

    public const int mapChunkSize = 241;
    [Range(0, 6)]
    public int levelOfDetail;
    public float noiseScale;

    public int octaves;
    [Range(0, 1)]
    public float persistance;
    public float lacunarity;

    public int seed;
    public Vector2 offset;

    public float meshHeightMultiplier;
    public AnimationCurve meshHeightCurve;

    public bool autoUpdate;

    public GameObject TreeGameObject;

    //reference this to set up the Levels.
    [Header("Terrain Heights")]
    public TerrainType[] regions;
    [Header("Trees")]
    [SerializeField]
    private float _FoliagePercent = 5f;

    [SerializeField]
    private Transform _FoliageParentObject;
    [SerializeField] private LayerMask _TreeMask;

    public GameObject CoinGameObject ;

    private void Start()
    {
        GenerateMap();
    }
    public void GenerateMap()
    {
		var GO  = GameObject.FindGameObjectsWithTag("Trees");
		foreach (var item in GO)
		{
			DestroyImmediate(item);
		}

        float[,] noiseMap = NoiseGenerator.GenerateNoiseMap(mapChunkSize, mapChunkSize, seed, noiseScale, octaves, persistance, lacunarity, offset);

        Color[] colourMap = new Color[mapChunkSize * mapChunkSize];
        for (int y = 0; y < mapChunkSize; y++)
        {
            for (int x = 0; x < mapChunkSize; x++)
            {
                float currentHeight = noiseMap[x, y];
                for (int i = 0; i < regions.Length; i++)
                {
                    if (currentHeight <= regions[i].height)
                    {
                        colourMap[y * mapChunkSize + x] = regions[i].colour;
                        if(Random.Range(0.0f, 100.0f) <= 0.1f)GameObject.Instantiate(CoinGameObject,new Vector3(y*10,Random.Range(10.0f,300.0f),x*10),Quaternion.identity,_FoliageParentObject);
                        if (i == 2)
                        {
                            if (Random.Range(0, 100.0f) <= _FoliagePercent)
                            {
                                var tree =  GameObject.Instantiate(TreeGameObject, new Vector3((y*10)+_FoliageParentObject.transform.position.y, 300.0f, (x*10)+_FoliageParentObject.transform.position.x), Quaternion.Euler(0,0,0), _FoliageParentObject);
                                RaycastHit hit;
                               if(Physics.Raycast(tree.transform.position,Vector3.down ,out hit,300.0f,_TreeMask))
                               {
                                    tree.transform.position = new Vector3(tree.transform.position.x,hit.transform.position.y+30.0f,tree.transform.position.z);
                               }
                            }
                        }
                        break;
                    }
                }
            }
        }

        MapOutput display = FindObjectOfType<MapOutput>();
        if (drawMode == DrawMode.NoiseMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(noiseMap));
        }
        else if (drawMode == DrawMode.ColourMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromColourMap(colourMap, mapChunkSize, mapChunkSize));
        }
        else if (drawMode == DrawMode.Mesh)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(noiseMap, meshHeightMultiplier, meshHeightCurve, levelOfDetail), TextureGenerator.TextureFromColourMap(colourMap, mapChunkSize, mapChunkSize));
        }
    }

    void OnValidate()
    {
        if (lacunarity < 1)
        {
            lacunarity = 1;
        }
        if (octaves < 0)
        {
            octaves = 0;
        }
    }
}

[System.Serializable]
public struct TerrainType
{
    public string name;
    public float height;
    public Color colour;
}