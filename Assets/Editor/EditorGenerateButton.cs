﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(MapGenerator)),CanEditMultipleObjects]
public class EditorGenerateButton : Editor
{
    public override void OnInspectorGUI()
    {
        MapGenerator mapgen = (MapGenerator)target;
        DrawDefaultInspector();
        if(GUILayout.Button("Generate Map"))
        {
            mapgen.GenerateMap();
        }
    }
}
