Emerging Tech Game Jam

Simple FlyOver game where you enjoy the landscape where you collect coins (collect 25 to win) and if you crash you lose.

You can move the plane using WASD.

In this Unity Project we used procedurally generated terrain using perlin noise, and the use of a weather API.

The game generates a terrain and activates particles systems depending on the weather of the provided location.

To change the location you can use the input that appears on the game screen, simply editing the text.

If the API doesn't recognize the city (it contains info about most cities) the default is Snow.

The terrain changes depending on the current weather on that city (real world, real time), if it's raining the water has a higher level, if it's snowing snow covers more terrain, etc. etc.

